
pub struct Tracked<T> {
    value: T,
    updated: bool,
}

impl<T: PartialEq> Tracked<T> {
    pub fn new(value: T) -> Self {
        Self { value, updated: false }
    }

    pub fn set(&mut self, value: T) {
        if value != self.value {
            self.value = value;
            self.updated = true;
        }
    }

    pub fn is_updated(&mut self) -> bool {
        let res = self.updated;
        self.updated = false;
        res
    }

    pub fn get(&mut self) -> &T {
        &self.value
    }

    pub fn get_if_updated(&mut self) -> Option<&T> {
        if self.is_updated() {
            Some(self.get())
        } else {
            None
        }
    }
}