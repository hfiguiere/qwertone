
pub mod mixer;
pub mod metronome;
pub mod synthesizer;

pub trait AudioSource {
    fn get_sample(&mut self) -> f32;
    fn process_messages(&mut self);
}
